package cocncurrency.practice;

import java.util.ArrayList;
import java.util.List;

public class HumanDataFactory {
    private HumanDataGenerator dataGenerator;

    public HumanDataFactory(HumanDataGenerator dataGenerator) {
        this.dataGenerator = dataGenerator;
    }

    public List<Human> getInitializedEntityList() {
        List<Human> humanList = new ArrayList<>();
        for (int i = 0; i < (HumanMainData.humanNames.length); i++) {
            Human humanForAddToList = new Human();
            humanList.add(getInitializedEntity(humanForAddToList, (i + 1)));
        }
        return humanList;
    }

    private Human getInitializedEntity(Human human, int idHuman) {
        human.setId(idHuman);
        human.setName(dataGenerator.getGeneratedHumanFieldData(HumanMainData.humanNames));
        human.setSex(dataGenerator.getGeneratedHumanFieldData(HumanMainData.humanSex));
        return human;
    }
}
