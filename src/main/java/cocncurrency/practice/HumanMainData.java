package cocncurrency.practice;

public class HumanMainData {
    public static String[] humanNames = {
            "Ivanko", "Toshlia", "Dimchik", "Slavko", "Pivkach", "Yobja", "Tanya", "Zoya", "Lyra", "Mark", "Valya",
            "Myron", "Artur", "Valera", "Den", "Nata", "Kolya", "Sasha", "Inna", "Max", "Dron", "Rysya", "Serj",
            "Mylana", "Myla", "John", "Yura", "Zhadan", "Ilarion", "Ulya", "Masha", "Anya", "Kyra", "Leha", "Fedya",
            "Pasha", "Vlad", "Danya", "Kyryl", "Zheka", "Slava", "Oleg", "Izya", "Slavomyr", "Alla", "Fyl", "Lenya",
            "Diana", "Nastys", "Ilona", "Ganna", "Gorpyna", "Orest", "Nadya", "Sofia", "Olga", "Larisa",
            "Ivanko", "Toshlia", "Dimchik", "Slavko", "Pivkach", "Yobja", "Tanya", "Zoya", "Lyra", "Mark", "Valya",
            "Myron", "Artur", "Valera", "Den", "Nata", "Kolya", "Sasha", "Inna", "Max", "Dron", "Rysya", "Serj",
            "Mylana", "Myla", "John", "Yura", "Zhadan", "Ilarion", "Ulya", "Masha", "Anya", "Kyra", "Leha", "Fedya",
            "Pasha", "Vlad", "Danya", "Kyryl", "Zheka", "Slava", "Oleg", "Izya", "Slavomyr", "Alla", "Fyl", "Lenya",
            "Diana", "Nastys", "Ilona", "Ganna", "Gorpyna", "Orest", "Nadya", "Sofia", "Olga", "Larisa",
            "Ivanko", "Toshlia", "Dimchik", "Slavko", "Pivkach", "Yobja", "Tanya", "Zoya", "Lyra", "Mark", "Valya",
            "Myron", "Artur", "Valera", "Den", "Nata", "Kolya", "Sasha", "Inna", "Max", "Dron", "Rysya", "Serj",
            "Mylana", "Myla", "John", "Yura", "Zhadan", "Ilarion", "Ulya", "Masha", "Anya", "Kyra", "Leha", "Fedya",
            "Pasha", "Vlad", "Danya", "Kyryl", "Zheka", "Slava", "Oleg", "Izya", "Slavomyr", "Alla", "Fyl", "Lenya",
            "Diana", "Nastys", "Ilona", "Ganna", "Gorpyna", "Orest", "Nadya", "Sofia", "Olga", "Larisa",
            "Ivanko", "Toshlia", "Dimchik", "Slavko", "Pivkach", "Yobja", "Tanya", "Zoya", "Lyra", "Mark", "Valya",
            "Myron", "Artur", "Valera", "Den", "Nata", "Kolya", "Sasha", "Inna", "Max", "Dron", "Rysya", "Serj",
            "Mylana", "Myla", "John", "Yura", "Zhadan", "Ilarion", "Ulya", "Masha", "Anya", "Kyra", "Leha", "Fedya",
            "Pasha", "Vlad", "Danya", "Kyryl", "Zheka", "Slava", "Oleg", "Izya", "Slavomyr", "Alla", "Fyl", "Lenya",
            "Diana", "Nastys", "Ilona", "Ganna", "Gorpyna", "Orest", "Nadya", "Sofia", "Olga", "Larisa"};

    public static String[] humanSex = {"male", "female"};
}
