package cocncurrency.practice;

public class Human {
    private String sex;
    private String name;
    private int id;

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Human`s data: [" +
                "sex: " + sex +
                ", name: " + name +
                ", id: " + id +
                "]";
    }
}
