package cocncurrency.practice;

public class HumanDataGenerator {
    public String getGeneratedHumanFieldData(String[] humanDataFieldArray) {
        int arraySize = humanDataFieldArray.length;
        int randomElementIndex = (int) (Math.random() * arraySize);
        return humanDataFieldArray[randomElementIndex];
    }
}
