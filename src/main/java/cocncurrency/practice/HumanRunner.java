package cocncurrency.practice;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HumanRunner {
    public static void main(String[] args) {

        HumanDataGenerator dataGenerator = new HumanDataGenerator();
        HumanDataFactory humanDataFactory = new HumanDataFactory(dataGenerator);
        List<Human> initializedEntityList = humanDataFactory.getInitializedEntityList();

        ExecutorService executor = Executors.newCachedThreadPool();
        TaskLimitSemaphore object = new TaskLimitSemaphore(executor, 3);

        try {
            object.submit(() -> {
                long startTime = System.nanoTime();
                for (int i = 0; i < initializedEntityList.size(); i++) {
                    initializedEntityList.get(i).getName();
                }
                long finishTime = System.nanoTime() - startTime;
                System.out.println("Classic iteration task1 is done! Elapsed time = " + finishTime + " nano seconds");
                return 1;
            });
            object.submit(() -> {
                long startTime = System.nanoTime();
                for (Human human : initializedEntityList) {
                    human.getName();
                }
                long finishTime = System.nanoTime() - startTime;
                System.out.println("For each iteration task2 is done! Elapsed time = " + finishTime  + " nano seconds");
                return 2;
            });
            object.submit(() -> {
                long startTime = System.nanoTime();
                initializedEntityList.forEach(Human::getName);
                long finishTime = System.nanoTime() - startTime;
                System.out.println("Java 8 for each iteration task3 is done! Elapsed time = " + finishTime  + " nano seconds");
                return 3;
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executor.shutdown();
    }
}
