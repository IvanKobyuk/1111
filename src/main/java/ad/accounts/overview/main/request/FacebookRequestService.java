package ad.accounts.overview.main.request;

import ad.accounts.overview.entity.TimeRangeType;
import ad.accounts.overview.main.utils.Utils;
import com.facebook.ads.sdk.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class FacebookRequestService {
    @Value("${facebook.api.app.secret}")
    private String appSecret;
    @Value("${facebook.api.access.token}")
    private String accessToken;
    @Autowired
    private Utils utilise;

    public APINodeList<AdAccount> getAdAccounts(String userId) {
        APIContext apiContext = new APIContext(accessToken, appSecret).enableDebug(false);
        User.APIRequestGetAdAccounts adAccountsRequest = null;
        APINodeList<AdAccount> adAccountsList = null;
        try {
            adAccountsRequest = new User(userId, apiContext)
                    .getAdAccounts()
                    .requestNameField()
                    .requestAccountIdField();
            adAccountsList = adAccountsRequest
                    .execute();
        } catch (APIException e) {
            e.printStackTrace();
        }
        return adAccountsList;
    }

    public APINodeList<AdsInsights> getAdAccountInsights(String adAccountId, int dayCount, TimeRangeType rangeType) {
        APIContext apiContext = new APIContext(accessToken, appSecret).enableDebug(false);
        AdAccount.APIRequestGetInsights adAccountInsightsRequest = null;
        String timeRange = getTimeRangeByType(rangeType, dayCount);
        APINodeList<AdsInsights> adsInsightsResponse = null;
        try {
            adAccountInsightsRequest = new AdAccount(adAccountId, apiContext)
                    .getInsights()
                    .setTimeRange(timeRange)
                    .requestField("spend")
                    .requestField("purchase_roas")
                    .requestField("action_values")
                    .requestField("cost_per_action_type")
                    .requestField("actions");
            adsInsightsResponse = adAccountInsightsRequest
                    .execute();
        } catch (APIException e) {
            e.printStackTrace();
        }
        return adsInsightsResponse;
    }

    public APINodeList<AdsInsights> getAdAccountNameAndCurrency(String adAccountId) {
        APIContext apiContext = new APIContext(accessToken, appSecret).enableDebug(false);
        AdAccount.APIRequestGetInsights adAccountInsightsRequest = null;
        APINodeList<AdsInsights> adsInsightsResponse = null;
        try {
            adAccountInsightsRequest = new AdAccount(adAccountId, apiContext)
                    .getInsights()
                    .requestField("account_name")
                    .requestField("account_currency");
            adsInsightsResponse = adAccountInsightsRequest
                    .execute();
        } catch (APIException e) {
            e.printStackTrace();
        }
        return adsInsightsResponse;
    }

    private String getTimeRangeByType(TimeRangeType type, int dayCount) {
        String timeRange = "No data";
        switch (type) {
            case ONE_DAY_RANGE:
                timeRange = utilise.getFormattedOneDateByDaysCount(dayCount);
                break;
            case CURRENT_RANGE:
                timeRange = utilise.getFormattedCurrentTimeRangeByDaysCount(dayCount);
                break;
            case PREVIOUS_RANGE:
                timeRange = utilise.getFormattedPreviousTimeRangeByDaysCount(dayCount);
                break;
            default:
                System.out.println("Unknown type!");
                break;
        }
        return timeRange;
    }
}
