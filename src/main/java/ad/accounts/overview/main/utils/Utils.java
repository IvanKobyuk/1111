package ad.accounts.overview.main.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@Slf4j
public class Utils {

    public String getFormattedOneDateByDaysCount(int dayCount) {
        LocalDate today = LocalDate.now();
        LocalDate oneDate = today.minusDays(dayCount);
        return "{since:'" + oneDate.toString() + "',until:'" + oneDate.toString() + "'}";  //get time range for one day
    }

    public String getFormattedCurrentTimeRangeByDaysCount(int dayCount) {
        LocalDate until = LocalDate.now().minusDays(1); //exclude today`s data
        LocalDate since = until.minusDays(dayCount - 1);
        return "{since:'" + since.toString() + "',until:'" + until.toString() + "'}";
    }

    public String getFormattedPreviousTimeRangeByDaysCount(int dayCount) {
        LocalDate until = LocalDate.now().minusDays(dayCount + 1); //exclude today`s data
        LocalDate since = until.minusDays(dayCount - 1);
        return "{since:'" + since.toString() + "',until:'" + until.toString() + "'}";
    }

}
