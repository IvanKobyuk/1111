package ad.accounts.overview.main.service;

import ad.accounts.overview.entity.AdAccountOverview;
import com.facebook.ads.sdk.APINodeList;
import com.facebook.ads.sdk.AdsInsights;

public class AdAccountOverviewInitializer {

    //initialize ad account name and currency into entity
    public void setEntityByAccountNameAndCurrency(APINodeList<AdsInsights> apiNodeList, AdAccountOverview entity) {
        for (AdsInsights adsInsights : apiNodeList) {
            initializeAdAccountNameAndCurrency(adsInsights, entity);
        }
    }

    private void initializeAdAccountNameAndCurrency(AdsInsights adsInsights, AdAccountOverview insights) {
        insights.setAdAccountName(adsInsights.getFieldAccountName());
        insights.setCurrency(adsInsights.getFieldAccountCurrency());
    }

    //yesterday
    public void initializeRoasYesterday(AdAccountMetricComparator.InsightComparedData data,
                                        AdAccountOverview insights) {
        insights.setRoasYesterday(data.getValue());
        insights.setRoasStatusYesterday(data.getStatus().toString());
        insights.setRoasPercentageYesterday(data.getChangesPercentage());
    }

    public void initializePurchaseYesterday(AdAccountMetricComparator.InsightComparedData data,
                                            AdAccountOverview insights) {
        insights.setPurchaseYesterday((long) data.getValue());
        insights.setPurchaseStatusYesterday(data.getStatus().toString());
        insights.setPurchasePercentageYesterday(data.getChangesPercentage());
    }

    public void initializeRevenueYesterday(AdAccountMetricComparator.InsightComparedData data,
                                           AdAccountOverview insights) {
        insights.setRevenueYesterday(data.getValue());
        insights.setRevenueStatusYesterday(data.getStatus().toString());
        insights.setRevenuePercentageYesterday(data.getChangesPercentage());
    }

    public void initializeCostYesterday(AdAccountMetricComparator.InsightComparedData data,
                                        AdAccountOverview insights) {
        insights.setCostPerPurchaseYesterday(data.getValue());
        insights.setCostPerPurchaseStatusYesterday(data.getStatus().toString());
        insights.setCostPerPurchasePercentageYesterday(data.getChangesPercentage());
    }

    public void initializeSpentYesterday(AdAccountMetricComparator.InsightComparedData data,
                                         AdAccountOverview insights) {
        insights.setSpentYesterday(data.getValue());
        insights.setSpentStatusYesterday(data.getStatus().toString());
        insights.setSpentPercentageYesterday(data.getChangesPercentage());
    }

    //last week
    public void initializeRoasLastWeek(AdAccountMetricComparator.InsightComparedData data,
                                       AdAccountOverview insights) {
        insights.setRoasLastWeek(data.getValue());
        insights.setRoasStatusLastWeek(data.getStatus().toString());
        insights.setRoasPercentageLastWeek(data.getChangesPercentage());
    }

    public void initializePurchaseLastWeek(AdAccountMetricComparator.InsightComparedData data,
                                           AdAccountOverview insights) {
        insights.setPurchaseLastWeek((long) data.getValue());
        insights.setPurchaseStatusLastWeek(data.getStatus().toString());
        insights.setPurchasePercentageLastWeek(data.getChangesPercentage());
    }

    public void initializeRevenueLastWeek(AdAccountMetricComparator.InsightComparedData data,
                                          AdAccountOverview insights) {
        insights.setRevenueLastWeek(data.getValue());
        insights.setRevenueStatusLastWeek(data.getStatus().toString());
        insights.setRevenuePercentageLastWeek(data.getChangesPercentage());
    }

    public void initializeCostLastWeek(AdAccountMetricComparator.InsightComparedData data,
                                       AdAccountOverview insights) {
        insights.setCostPerPurchaseLastWeek(data.getValue());
        insights.setCostPerPurchaseStatusLastWeek(data.getStatus().toString());
        insights.setCostPerPurchasePercentageLastWeek(data.getChangesPercentage());
    }

    public void initializeSpentLastWeek(AdAccountMetricComparator.InsightComparedData data,
                                        AdAccountOverview insights) {
        insights.setSpentLastWeek(data.getValue());
        insights.setSpentStatusLastWeek(data.getStatus().toString());
        insights.setSpentPercentageLastWeek(data.getChangesPercentage());
    }

    //last two weeks
    public void initializeRoasLastTwoWeeks(AdAccountMetricComparator.InsightComparedData data,
                                           AdAccountOverview insights) {
        insights.setRoasLastTwoWeeks(data.getValue());
        insights.setRoasStatusLastTwoWeeks(data.getStatus().toString());
        insights.setRoasPercentageLastTwoWeeks(data.getChangesPercentage());
    }

    public void initializePurchaseLastTwoWeeks(AdAccountMetricComparator.InsightComparedData data,
                                               AdAccountOverview insights) {
        insights.setPurchaseLastTwoWeeks((long) data.getValue());
        insights.setPurchaseStatusLastTwoWeeks(data.getStatus().toString());
        insights.setPurchasePercentageLastTwoWeeks(data.getChangesPercentage());
    }

    public void initializeRevenueLastTwoWeeks(AdAccountMetricComparator.InsightComparedData data,
                                              AdAccountOverview insights) {
        insights.setRevenueLastTwoWeeks(data.getValue());
        insights.setRevenueStatusLastTwoWeeks(data.getStatus().toString());
        insights.setRevenuePercentageLastTwoWeeks(data.getChangesPercentage());
    }

    public void initializeCostLastTwoWeeks(AdAccountMetricComparator.InsightComparedData data,
                                           AdAccountOverview insights) {
        insights.setCostPerPurchaseLastTwoWeeks(data.getValue());
        insights.setCostPerPurchaseStatusLastTwoWeeks(data.getStatus().toString());
        insights.setCostPerPurchasePercentageLastTwoWeeks(data.getChangesPercentage());
    }

    public void initializeSpentLastTwoWeeks(AdAccountMetricComparator.InsightComparedData data,
                                            AdAccountOverview insights) {
        insights.setSpentLastTwoWeeks(data.getValue());
        insights.setSpentStatusLastTwoWeeks(data.getStatus().toString());
        insights.setSpentPercentageLastTwoWeeks(data.getChangesPercentage());
    }

    //in progress write setters with no data for empty ad accounts
}
