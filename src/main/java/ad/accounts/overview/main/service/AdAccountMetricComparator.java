package ad.accounts.overview.main.service;

import ad.accounts.overview.entity.MonitoredMetricStatus;

public class AdAccountMetricComparator {


    public InsightComparedData compareMetricsByValue(double currentMetricValue, double previousMetricValue) {
        InsightComparedData data = new InsightComparedData();
        data.setValue(currentMetricValue);

        if (isBiggerCurrentMetricValue(currentMetricValue, previousMetricValue)
                && !isZeroMetricValue(previousMetricValue)) {
            data.setStatus(MonitoredMetricStatus.UP_RISE);
            int percentage = (int)
                    (((currentMetricValue / previousMetricValue) * 100) - 100);
            data.setChangesPercentage(percentage);
        } else if (isBiggerCurrentMetricValue(currentMetricValue, previousMetricValue)
                && isZeroMetricValue(previousMetricValue)) {
            data.setStatus(MonitoredMetricStatus.UP_RISE);
            int percentage = (int) ((currentMetricValue * 100) - 100);
            data.setChangesPercentage(percentage);

        } else if (isBiggerPreviousMetricValue(currentMetricValue, previousMetricValue)
                && !isZeroMetricValue(currentMetricValue)) {
            data.setStatus(MonitoredMetricStatus.DOWN_DROP);
            int percentage = (int)
                    (100 - ((currentMetricValue / previousMetricValue) * 100));
            data.setChangesPercentage(percentage);
        } else if (isBiggerPreviousMetricValue(currentMetricValue, previousMetricValue)
                && isZeroMetricValue(currentMetricValue)) {
            data.setStatus(MonitoredMetricStatus.DOWN_DROP);
            int percentage = 100;   //cause if we have insightsCurrentPeriod = 0.0, then our percentage always will be 100%
            data.setChangesPercentage(percentage);

        } else {
            data.setStatus(MonitoredMetricStatus.NONE);
            data.setChangesPercentage(0);
        }
        return data;
    }

    private boolean isBiggerCurrentMetricValue(double currentMetricValue, double previousMetricValue) {
        boolean response = true;
        if (currentMetricValue <= previousMetricValue) {
            response = false;
        }
        return response;
    }

    private boolean isBiggerPreviousMetricValue(double currentMetricValue, double previousMetricValue) {
        boolean response = true;
        if (previousMetricValue <= currentMetricValue) {
            response = false;
        }
        return response;
    }

    private boolean isZeroMetricValue(double metricValue) {
        boolean response = true;
        if (metricValue != 0.0) {
            response = false;
        }
        return response;
    }

    public static class InsightComparedData {
        double value;
        int changesPercentage;
        MonitoredMetricStatus status;

        public double getValue() {
            return value;
        }

        public void setValue(double value) {
            this.value = value;
        }

        public int getChangesPercentage() {
            return changesPercentage;
        }

        public void setChangesPercentage(int changesPercentage) {
            this.changesPercentage = changesPercentage;
        }

        public MonitoredMetricStatus getStatus() {
            return status;
        }

        public void setStatus(MonitoredMetricStatus status) {
            this.status = status;
        }
    }
}
