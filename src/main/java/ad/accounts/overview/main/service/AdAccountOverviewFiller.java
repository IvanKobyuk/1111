package ad.accounts.overview.main.service;

import ad.accounts.overview.entity.AdAccountOverview;

import ad.accounts.overview.entity.TimeRangeType;
import ad.accounts.overview.main.request.FacebookRequestService;
import com.facebook.ads.sdk.APINodeList;
import com.facebook.ads.sdk.AdAccount;
import com.facebook.ads.sdk.AdsActionStats;
import com.facebook.ads.sdk.AdsInsights;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class AdAccountOverviewFiller {

    @Autowired
    private FacebookRequestService requestService;

    @Autowired
    private AdAccountMetricComparator comparator;

    @Autowired
    private AdAccountOverviewInitializer initializer;

    public void getAdAccountsOverview(String userId) {
        APINodeList<AdAccount> adAccounts = requestService.getAdAccounts(userId);
        List<AdAccountOverview> adAccountOverviewList = new ArrayList<>();
        for (AdAccount adAccount : adAccounts) {
            //in progress
        }
    }

    private String getAdAccountId(APINodeList<AdAccount> adAccounts) {
        String adAccountId = "no_data";
        for (AdAccount adAccount : adAccounts) {
            adAccountId = adAccount.getFieldAccountId();
        }
        return adAccountId;
    }

    public AdAccountOverview getFilledEntity(String adAccountId) throws JSONException {

        APINodeList<AdsInsights> nodeListWithNameAndCurrency = requestService.getAdAccountNameAndCurrency(adAccountId);

        APINodeList<AdsInsights> nodeListYesterday = requestService.getAdAccountInsights(
                adAccountId,
                1,   //yesterday
                TimeRangeType.ONE_DAY_RANGE);

        APINodeList<AdsInsights> nodeListBeforeYesterday = requestService.getAdAccountInsights(
                adAccountId,
                2,   //before yesterday
                TimeRangeType.ONE_DAY_RANGE);

        APINodeList<AdsInsights> nodeListCurrentLastWeek = requestService.getAdAccountInsights(
                adAccountId,
                7, //current last week
                TimeRangeType.CURRENT_RANGE);

        APINodeList<AdsInsights> nodeListPreviousLastWeek = requestService.getAdAccountInsights(
                adAccountId,
                7, //previous last week
                TimeRangeType.PREVIOUS_RANGE);

        APINodeList<AdsInsights> nodeListCurrentLastTwoWeeks = requestService.getAdAccountInsights(
                adAccountId,
                14, //current last 2 weeks
                TimeRangeType.CURRENT_RANGE);

        APINodeList<AdsInsights> nodeListPreviousLastTwoWeeks = requestService.getAdAccountInsights(
                adAccountId,
                14, //previous last 2 weeks
                TimeRangeType.PREVIOUS_RANGE);

        JSONObject yesterday = getJsonFromAPINodeList(nodeListYesterday);
        yesterday.put("dateRangeType", "YESTERDAY");
        JSONObject beforeYesterday = getJsonFromAPINodeList(nodeListBeforeYesterday);

        JSONObject currentLastWeek = getJsonFromAPINodeList(nodeListCurrentLastWeek);
        currentLastWeek.put("dateRangeType", "LAST_WEEK");
        JSONObject previousLastWeek = getJsonFromAPINodeList(nodeListPreviousLastWeek);

        JSONObject currentLastTwoWeeks = getJsonFromAPINodeList(nodeListCurrentLastTwoWeeks);
        currentLastTwoWeeks.put("dateRangeType", "LAST_TWO_WEEKS");
        JSONObject previousLastTwoWeeks = getJsonFromAPINodeList(nodeListPreviousLastTwoWeeks);

        AdAccountOverview overviewEntity = new AdAccountOverview();
        initializer.setEntityByAccountNameAndCurrency(nodeListWithNameAndCurrency, overviewEntity);
        setEntityByParams(yesterday, beforeYesterday, getFieldsNameList(), overviewEntity);
        setEntityByParams(currentLastWeek, previousLastWeek, getFieldsNameList(), overviewEntity);
        setEntityByParams(currentLastTwoWeeks, previousLastTwoWeeks, getFieldsNameList(), overviewEntity);

        return overviewEntity;
    }

    private void setEntityByParams(JSONObject current,
                                   JSONObject previous,
                                   List<String> fieldNamesList,
                                   AdAccountOverview insights) throws JSONException {

        for (String fieldName : fieldNamesList) {
            AdAccountMetricComparator.InsightComparedData comparedData = getComparedData(
                    current,
                    previous,
                    fieldName);

            if (isRoasField(fieldName)) {
                if (current.getString("dateRangeType").equals("YESTERDAY")) {
                    initializer.initializeRoasYesterday(comparedData, insights);
                } else if (current.getString("dateRangeType").equals("LAST_WEEK")) {
                    initializer.initializeRoasLastWeek(comparedData, insights);
                } else {
                    initializer.initializeRoasLastTwoWeeks(comparedData, insights);
                }
            }

            if (isPurchaseField(fieldName)) {
                if (current.getString("dateRangeType").equals("YESTERDAY")) {
                    initializer.initializePurchaseYesterday(comparedData, insights);
                } else if (current.getString("dateRangeType").equals("LAST_WEEK")) {
                    initializer.initializePurchaseLastWeek(comparedData, insights);
                } else {
                    initializer.initializePurchaseLastTwoWeeks(comparedData, insights);
                }
            }

            if (isRevenueField(fieldName)) {
                if (current.getString("dateRangeType").equals("YESTERDAY")) {
                    initializer.initializeRevenueYesterday(comparedData, insights);
                } else if (current.getString("dateRangeType").equals("LAST_WEEK")) {
                    initializer.initializeRevenueLastWeek(comparedData, insights);
                } else {
                    initializer.initializeRevenueLastTwoWeeks(comparedData, insights);
                }
            }

            if (isCostField(fieldName)) {
                if (current.getString("dateRangeType").equals("YESTERDAY")) {
                    initializer.initializeCostYesterday(comparedData, insights);
                } else if (current.getString("dateRangeType").equals("LAST_WEEK")) {
                    initializer.initializeCostLastWeek(comparedData, insights);
                } else {
                    initializer.initializeCostLastTwoWeeks(comparedData, insights);
                }
            }

            if (isSpentField(fieldName)) {
                if (current.getString("dateRangeType").equals("YESTERDAY")) {
                    initializer.initializeSpentYesterday(comparedData, insights);
                } else if (current.getString("dateRangeType").equals("LAST_WEEK")) {
                    initializer.initializeSpentLastWeek(comparedData, insights);
                } else {
                    initializer.initializeSpentLastTwoWeeks(comparedData, insights);
                }
            }
        }
    }

    private AdAccountMetricComparator.InsightComparedData getComparedData(JSONObject current,
                                                                          JSONObject previous,
                                                                          String fieldName) {
        String currentValue = getFieldsValueFromJson(current, fieldName);
        String previousValue = getFieldsValueFromJson(previous, fieldName);
        return comparator.compareMetricsByValue(Double.parseDouble(currentValue), Double.parseDouble(previousValue));
    }

    private JSONObject getJsonFromAPINodeList(APINodeList<AdsInsights> apiNodeList) throws JSONException {
        JSONObject customAdActionsJson = new JSONObject();
        if (!apiNodeList.isEmpty()) {
            try {
                customAdActionsJson = getActionsDataFromAdInsights(apiNodeList);
            } catch (JSONException e) {
                log.warn("Error occurred while getting json object data from apiNodeList, {}", apiNodeList);
            }
        } else {
            customAdActionsJson.put("purchaseRoas", "0.0");
            customAdActionsJson.put("purchase", "0.0");
            customAdActionsJson.put("revenue", "0.0");
            customAdActionsJson.put("costPerPurchase", "0.0");
            customAdActionsJson.put("amountSpend", "0.0");
        }
        return customAdActionsJson;
    }

    private String getFieldsValueFromJson(JSONObject customAdActionsJson, String fieldName) {
        String valueField = "no_data_from_json";
        try {
            valueField = customAdActionsJson.getString(fieldName);
        } catch (JSONException e) {
            log.warn("Error occurred during getting data from Json, {} by such field, {}",
                    customAdActionsJson, fieldName);
        }
        return valueField;
    }

    private JSONObject getActionsDataFromAdInsights(APINodeList<AdsInsights> insightsAPINodeList) throws JSONException {
        JSONObject customAdActionsJson = new JSONObject();
        String purchaseRoas = "0.0";
        String purchase = "0.0";
        String revenue = "0.0";
        String costPerPurchase = "0.0";
        String amountSpend = "0.0";

        for (AdsInsights insight : insightsAPINodeList) {
            purchaseRoas = getValueFromAdsActionStatsByField(insight.getFieldPurchaseRoas());
            purchase = getValueFromAdsActionStatsByField(insight.getFieldActions());
            revenue = getValueFromAdsActionStatsByField(insight.getFieldActionValues());
            costPerPurchase = getValueFromAdsActionStatsByField(insight.getFieldCostPerActionType());
            amountSpend = insight.getFieldSpend();
        }

        if (!purchaseRoas.equals("no_data")) {
            customAdActionsJson.put("purchaseRoas", purchaseRoas);
        } else {
            customAdActionsJson.put("purchaseRoas", purchaseRoas);
        }
        if (!purchase.equals("no_data")) {
            customAdActionsJson.put("purchase", purchase);
        } else {
            customAdActionsJson.put("purchase", purchase);
        }
        if (!purchase.equals("no_data")) {
            customAdActionsJson.put("revenue", revenue);
        } else {
            customAdActionsJson.put("revenue", revenue);
        }
        if (!purchase.equals("no_data")) {
            customAdActionsJson.put("costPerPurchase", costPerPurchase);
        } else {
            customAdActionsJson.put("costPerPurchase", costPerPurchase);
        }
        if (!amountSpend.equals("null")) {  //check this condition
            customAdActionsJson.put("amountSpend", amountSpend);
        } else {
            customAdActionsJson.put("amountSpend", amountSpend);
        }
        return customAdActionsJson;
    }

    private String getValueFromAdsActionStatsByField(List<AdsActionStats> adsActionStatsList) {
        String fieldValueFromList = "no_data";
        for (AdsActionStats elem : adsActionStatsList) {
            if (elem.toString().contains("offsite_conversion.fb_pixel_purchase")) {
                fieldValueFromList = elem.getFieldValue();
            } else if (elem.toString().contains("omni_purchase")) {
                fieldValueFromList = elem.getFieldValue();
            }
        }
        return fieldValueFromList;
    }

    boolean isRoasField(String fieldName) {
        boolean response = true;
        if (!fieldName.equals("purchaseRoas")) {
            response = false;
        }
        return response;
    }

    boolean isPurchaseField(String fieldName) {
        boolean response = true;
        if (!fieldName.equals("purchase")) {
            response = false;
        }
        return response;
    }

    boolean isRevenueField(String fieldName) {
        boolean response = true;
        if (!fieldName.equals("revenue")) {
            response = false;
        }
        return response;
    }

    boolean isCostField(String fieldName) {
        boolean response = true;
        if (!fieldName.equals("costPerPurchase")) {
            response = false;
        }
        return response;
    }

    boolean isSpentField(String fieldName) {
        boolean response = true;
        if (!fieldName.equals("amountSpend")) {
            response = false;
        }
        return response;
    }

    // some useful hardcode
    private List<String> getFieldsNameList() {
        return new ArrayList<>(Arrays.asList(
                "purchaseRoas",
                "purchase",
                "revenue",
                "costPerPurchase",
                "amountSpend"));
    }
}
