package ad.accounts.overview.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ad_account_overview")
public class AdAccountOverview {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String adAccountName;

    @Column
    private String currency;

    /**
     * fields for yesterday time range
     */
    @Column
    private double roasYesterday;

    @Column
    private int roasPercentageYesterday;

    @Column
    private String roasStatusYesterday;

    @Column
    private double spentYesterday;

    @Column
    private int spentPercentageYesterday;

    @Column
    private String spentStatusYesterday;

    @Column
    private long purchaseYesterday;

    @Column
    private int purchasePercentageYesterday;

    @Column
    private String purchaseStatusYesterday;

    @Column
    private double revenueYesterday;

    @Column
    private int revenuePercentageYesterday;

    @Column
    private String revenueStatusYesterday;

    @Column
    double costPerPurchaseYesterday;

    @Column
    private int costPerPurchasePercentageYesterday;

    @Column
    private String costPerPurchaseStatusYesterday;

    /**
     * fields for last week time range
     */
    @Column
    private double roasLastWeek;

    @Column
    private int roasPercentageLastWeek;

    @Column
    private String roasStatusLastWeek;

    @Column
    private double spentLastWeek;

    @Column
    private int spentPercentageLastWeek;

    @Column
    private String spentStatusLastWeek;

    @Column
    private long purchaseLastWeek;

    @Column
    private int purchasePercentageLastWeek;

    @Column
    private String purchaseStatusLastWeek;

    @Column
    private double revenueLastWeek;

    @Column
    private int revenuePercentageLastWeek;

    @Column
    private String revenueStatusLastWeek;

    @Column
    double costPerPurchaseLastWeek;

    @Column
    private int costPerPurchasePercentageLastWeek;

    @Column
    private String costPerPurchaseStatusLastWeek;

    /**
     * fields for last two weeks time range
     */
    @Column
    private double roasLastTwoWeeks;

    @Column
    private int roasPercentageLastTwoWeeks;

    @Column
    private String roasStatusLastTwoWeeks;

    @Column
    private double spentLastTwoWeeks;

    @Column
    private int spentPercentageLastTwoWeeks;

    @Column
    private String spentStatusLastTwoWeeks;

    @Column
    private long purchaseLastTwoWeeks;

    @Column
    private int purchasePercentageLastTwoWeeks;

    @Column
    private String purchaseStatusLastTwoWeeks;

    @Column
    private double revenueLastTwoWeeks;

    @Column
    private int revenuePercentageLastTwoWeeks;

    @Column
    private String revenueStatusLastTwoWeeks;

    @Column
    double costPerPurchaseLastTwoWeeks;

    @Column
    private int costPerPurchasePercentageLastTwoWeeks;

    @Column
    private String costPerPurchaseStatusLastTwoWeeks;
}
