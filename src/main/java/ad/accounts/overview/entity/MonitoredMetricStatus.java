package ad.accounts.overview.entity;

public enum MonitoredMetricStatus {
    UP_RISE,
    DOWN_DROP,
    NONE
}
