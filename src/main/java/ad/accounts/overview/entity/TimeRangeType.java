package ad.accounts.overview.entity;

public enum TimeRangeType {
    //time range commands
    ONE_DAY_RANGE,
    CURRENT_RANGE,
    PREVIOUS_RANGE
}
