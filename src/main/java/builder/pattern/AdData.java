package builder.pattern;

public class AdData {

    private final String creative;
    private final String fbPage;
    private final String primaryText;
    private final String websiteURL;
    private final String callToActionButton;

    private final String urlParameters;
    private final String headline;
    private final String description;
    private final String displayLink;

    @Override
    public String toString() {
        return "AdData{" +
                "creative='" + creative + '\'' +
                ", fbPage='" + fbPage + '\'' +
                ", primaryText='" + primaryText + '\'' +
                ", websiteURL='" + websiteURL + '\'' +
                ", callToActionButton='" + callToActionButton + '\'' +
                ", urlParameters='" + urlParameters + '\'' +
                ", headline='" + headline + '\'' +
                ", description='" + description + '\'' +
                ", displayLink='" + displayLink + '\'' +
                '}';
    }

    public static class Builder {
        private final String creative;
        private final String fbPage;
        private final String primaryText;
        private final String websiteURL;
        private final String callToActionButton;

        private String headline = "";
        private String description = "";
        private String displayLink = "";
        private String urlParameters = "";

        public Builder(String creative, String fbPage, String primaryText, String websiteURL, String callToActionButton) {
            this.creative = creative;
            this.fbPage = fbPage;
            this.primaryText = primaryText;
            this.websiteURL = websiteURL;
            this.callToActionButton = callToActionButton;
        }

        public Builder headline(String text) {
            headline = text;
            return this;
        }

        public Builder description(String text) {
            description = text;
            return this;
        }

        public Builder displayLink(String text) {
            displayLink = text;
            return this;
        }

        public Builder urlParameters(String text) {
            urlParameters = text;
            return this;
        }

        public AdData build() {
            return new AdData(this);
        }
    }

    private AdData(Builder builder) {
        creative = builder.creative;
        fbPage = builder.fbPage;
        primaryText = builder.primaryText;
        websiteURL = builder.websiteURL;
        callToActionButton = builder.callToActionButton;
        headline = builder.headline;
        description = builder.description;
        displayLink = builder.displayLink;
        urlParameters = builder.displayLink;
    }
}

