package builder.pattern;

public class AdDataBuilder {

    public AdData builder() {
        AdData newAd = new AdData.Builder(
                "imageUrl",
                "Decidyxa Page",
                "Text for ad",
                "https://youtu.be/pzvWy0rpTSc",
                "Learn More")
                .description("some description text")
                .displayLink("link that will display in post")
                .headline("headline")
                .urlParameters("?utm_medium={{adset.name}}")
                .build();
        return newAd;
    }
}
