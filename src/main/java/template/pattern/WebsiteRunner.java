package template.pattern;

public class WebsiteRunner {
    public void runAllPages() {
        WebsiteTemplate mainPage = new WebsiteMainPage();
        WebsiteTemplate blogPage = new WebsiteBlogPage();

        mainPage.showAllPartsOfPage();
        System.out.println("\n||||||||||||||||||||||||||||||||||||||||||||\n");
        blogPage.showAllPartsOfPage();
    }
}
