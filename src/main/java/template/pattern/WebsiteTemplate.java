package template.pattern;

public abstract class WebsiteTemplate {

    //template method, final so subclasses can't override
    public final void showAllPartsOfPage() {
        showHeaderOfPage();
        showCustomPartOfPage();
        if (isActiveAdsContentOnPage()) {
            showAdsContentOnPage();
        }
        showFooterOfPage();
        showCreditsOfPage();
    }

    //default methods
    private void showHeaderOfPage() {
        System.out.println("Page`s Header");
    }

    private void showFooterOfPage() {
        System.out.println("Page`s Footer");
    }

    private void showCreditsOfPage() {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<");
        System.out.println("All rights belongs to Ivan Devtestenko Inc.");
    }

    //methods to be implemented by subclasses
    public abstract void showCustomPartOfPage();

    // hook method
    boolean isActiveAdsContentOnPage() {
        return true;
    }

    void showAdsContentOnPage() {
        System.out.println("***************AD CONTENT****************");
        System.out.println("Only our company can excite your with MEGA X-Treme SHOW on BIKES!");
    }
}
