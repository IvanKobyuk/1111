package template.pattern;

public class WebsiteMainPage extends WebsiteTemplate {

    @Override
    public void showCustomPartOfPage() {
        System.out.println("Here is a main part of page");
    }

    public boolean isActiveAdsContentOnPage() {
        return false;
    }
}
