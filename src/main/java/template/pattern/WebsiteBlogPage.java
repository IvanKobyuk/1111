package template.pattern;

public class WebsiteBlogPage extends WebsiteTemplate {
    @Override
    public void showCustomPartOfPage() {
        System.out.println("Here is a blog part of page");
    }
}
