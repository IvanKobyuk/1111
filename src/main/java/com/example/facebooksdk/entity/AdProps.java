package com.example.facebooksdk.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "ad_props")
public class AdProps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String adName;

    @Column
    private String adId;
}
