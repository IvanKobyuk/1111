package com.example.facebooksdk.entity;

import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@Table(name = "insights_props")
public class InsightsProps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String adName;

    private String adId;

    private String campaignName;

    private String adAccountName;

    private String clicks;

    private String impressions;

    private String spend;

    private String insightsStatus;
}