package com.example.facebooksdk.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "creative_props")
public class CreativeProps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String adId;

    private String adName;

    private String creativeBody;

    private String creativeType;

    private String imageUrl;

}
