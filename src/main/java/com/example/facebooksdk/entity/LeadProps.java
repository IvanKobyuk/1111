package com.example.facebooksdk.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "lead_props")
public class LeadProps {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String adId;

    private String adName;

    private String createdTime;

    private String fieldData;

}
