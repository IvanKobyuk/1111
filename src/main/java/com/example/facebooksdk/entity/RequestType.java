package com.example.facebooksdk.entity;

public enum RequestType {
    GET_ADACCOUNTS,
    GET_INSIGHTS,
    GET_ADS,
    GET_AD_CREATIVE_DATA,
    GET_AD_CREATIVE_DATA_BY_ID,

    //table commands
    ADS,
    INSIGHTS,
    CREATIVES,
    ALL,

    //database commands
    INITIALIZE_ALL_PROPS_TO_DATABASE,
    INITIALIZE_LEAD_PROPS_TO_DATABASE,
    GET_ADS_PROPS,
    GET_INSIGHTS_PROPS,
    GET_CREATIVE_PROPS,
    GET_All_PROPS,
    GET_CERTAIN_PROPS,
    GET_LEAD_PROPS,


    //time range commands
    ONE_DAY_RANGE,
    CURRENT_RANGE,
    PREVIOUS_RANGE

}
