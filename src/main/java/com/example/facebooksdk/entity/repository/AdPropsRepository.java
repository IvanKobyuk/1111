package com.example.facebooksdk.entity.repository;

import com.example.facebooksdk.entity.AdProps;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Repository
public interface AdPropsRepository extends JpaRepository<AdProps, Long> {
    AdProps findOneByAdName(String adName);

    @Override
    List<AdProps> findAll();
}


