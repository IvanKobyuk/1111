package com.example.facebooksdk.entity.repository;

import com.example.facebooksdk.entity.CreativeProps;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CreativePropsRepository extends JpaRepository<CreativeProps, Long> {
    List<CreativeProps> findAll();
}
