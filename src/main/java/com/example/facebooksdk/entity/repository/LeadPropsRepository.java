package com.example.facebooksdk.entity.repository;

import com.example.facebooksdk.entity.LeadProps;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LeadPropsRepository extends JpaRepository<LeadProps, Long> {
    List<LeadProps> findAll();
}
