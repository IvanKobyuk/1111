package com.example.facebooksdk.entity.repository;

import com.example.facebooksdk.entity.InsightsProps;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface InsightsPropsRepository extends JpaRepository<InsightsProps, Long> {
    List<InsightsProps> findAll();

}
