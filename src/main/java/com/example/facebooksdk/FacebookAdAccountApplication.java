package com.example.facebooksdk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FacebookAdAccountApplication {

    public static void main(String[] args) {
        SpringApplication.run(FacebookAdAccountApplication.class, args);
    }

}
