package com.example.facebooksdk.main.service;

import com.example.facebooksdk.entity.AdProps;
import com.example.facebooksdk.entity.CreativeProps;
import com.example.facebooksdk.entity.InsightsProps;
import com.example.facebooksdk.entity.LeadProps;
import com.example.facebooksdk.main.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class HtmlConverter {

    @Autowired
    private Utils utils;

    public String convertAdPropsListToHtmlTable(List<AdProps> adPropsList) {
        String adPropsElemToHtmlTable = "";
        String tableHeader =
                "<html>" +
                        "<head>" +
                        "<style>" +
                        "table, th, td {" +
                        "border: " + "1px solid black;" + "}" +
                        "</style>" +
                        "</head>" +
                        "<body>" +
                        "<h2>AdProps Table</h2>" +
                        "<table style=" + "width:100%" + ">" +
                        "<tr>" +
                        "<th>" + "Ad Id" + "</th>" +
                        "<th>" + "Ad Name" + "</th>" +
                        "</tr>";

        String tableEnd =
                "</table>" +
                        "</body>" +
                        "</html>";

        if (adPropsList != null) {
            for (AdProps adProps : adPropsList) {
                adPropsElemToHtmlTable +=
                        "<tr>" +
                                "<td>" + adProps.getAdId() + "</td>" +
                                "<td>" + adProps.getAdName() + "</td>" +
                                "</tr>";
            }
        }
        return tableHeader + adPropsElemToHtmlTable + tableEnd;
    }

    public String convertInsightsPropsListToHtmlTable(List<InsightsProps> insightsPropsList) {
        String insightsPropsToHtmlTable = "";
        String tableHeader =
                "<html>" +
                        "<head>" +
                        "<style>" +
                        "table, th, td {" +
                        "border: " + "1px solid black;" + "}" +
                        "</style>" +
                        "</head>" +
                        "<body>" +
                        "<h2>InsightsProps Table</h2>" +
                        "<table style=" + "width:100%" + ">" +
                        "<tr>" +
                        "<th>" + "Insights Status" + "</th>" +
                        "<th>" + "AdAccount Name/Id" + "</th>" +
                        "<th>" + "Campaign Name/Id" + "</th>" +
                        "<th>" + "Ad Name" + "</th>" +
                        "<th>" + "Clicks" + "</th>" +
                        "<th>" + "Impressions" + "</th>" +
                        "<th>" + "Spend" + "</th>" +
                        "</tr>";
        String tableEnd =
                "</table>" +
                        "</body>" +
                        "</html>";

        if (insightsPropsList != null) {
            for (InsightsProps insightsProps : insightsPropsList) {
                insightsPropsToHtmlTable +=
                        "<tr>" +
                                "<td>" + insightsProps.getInsightsStatus() + "</td>" +
                                "<td>" + insightsProps.getAdAccountName() + "</td>" +
                                "<td>" + insightsProps.getCampaignName() + "</td>" +
                                "<td>" + insightsProps.getAdName() + "</td>" +
                                "<td>" + insightsProps.getClicks() + "</td>" +
                                "<td>" + insightsProps.getImpressions() + "</td>" +
                                "<td>" + insightsProps.getSpend() + "</td>" +
                                "</tr>";
            }
        }
        return tableHeader + insightsPropsToHtmlTable + tableEnd;
    }

    public String convertCreativePropsListToHtmlTable(List<CreativeProps> creativePropsList) {
        String creativePropsToHtmlTable = "";
        String tableHeader =
                "<html>" +
                        "<head>" +
                        "<style>" +
                        "table, th, td {" +
                        "border: " + "1px solid black;" + "}" +
                        "</style>" +
                        "</head>" +
                        "<body>" +
                        "<h2>CreativeProps Table</h2>" +
                        "<table style=" + "width:100%" + ">" +
                        "<tr>" +
                        "<th>" + "Id" + "</th>" +
                        "<th>" + "Ad Name" + "</th>" +
                        "<th>" + "Creative Body" + "</th>" +
                        "<th>" + "Creative Type" + "</th>" +
                        "<th>" + "Image Url" + "</th>" +
                        "</tr>";
        String tableEnd =
                "</table>" +
                        "</body>" +
                        "</html>";

        if (creativePropsList != null) {
            for (CreativeProps creativeProps : creativePropsList) {
                creativePropsToHtmlTable +=
                        "<tr>" +
                                "<td>" + creativeProps.getId() + "</td>" +
                                "<td>" + creativeProps.getAdName() + "</td>" +
                                "<td>" + creativeProps.getCreativeBody() + "</td>" +
                                "<td>" + creativeProps.getCreativeType() + "</td>" +
                                "<td>" + creativeProps.getImageUrl() + "</td>" +
                                "</tr>";
            }
        }
        return tableHeader + creativePropsToHtmlTable + tableEnd;
    }

    public String convertAllPropsToHtmlTable(List<AdProps> adPropsList
            , List<InsightsProps> insightsPropsList
            , List<CreativeProps> creativePropsList) {
        String elemAllPropsToHtmlTable = "";
        String tableHeader =
                "<html>" +
                        "<head>" +
                        "<style>" +
                        "table, th, td {" +
                        "border: " + "1px solid black;" + "}" +
                        "</style>" +
                        "</head>" +
                        "<body>" +
                        "<h2>AdProps with Insights and Creative Table</h2>" +
                        "<table style=" + "width:100%" + ">" +
                        "<tr>" +
                        "<th>" + "Ad Id" + "</th>" +
                        "<th>" + "Ad Name" + "</th>" +
                        "<th>" + "AdAccount Name/Id" + "</th>" +
                        "<th>" + "Campaign Name/Id" + "</th>" +
                        "<th>" + "Insights Status" + "</th>" +
                        "<th>" + "Clicks" + "</th>" +
                        "<th>" + "Impressions" + "</th>" +
                        "<th>" + "Spend" + "</th>" +
                        "<th>" + "Creative Body" + "</th>" +
                        "<th>" + "Creative Type" + "</th>" +
                        "<th>" + "Image Url" + "</th>" +
                        "</tr>";
        String tableEnd =
                "</table>" +
                        "</body>" +
                        "</html>";

        for (AdProps adProps : adPropsList) {
            String adId = adProps.getAdId();
            InsightsProps insightsProps = utils.getInsightsPropsElemFromListByAdId(adId, insightsPropsList);
            CreativeProps creativeProps = utils.getCreativePropsElemFromListByAdId(adId, creativePropsList);

            elemAllPropsToHtmlTable +=
                    "<tr>" +
                            "<td>" + adProps.getId() + "</td>" +
                            "<td>" + adProps.getAdName() + "</td>" +
                            "<td>" + insightsProps.getAdAccountName() + "</td>" +
                            "<td>" + insightsProps.getCampaignName() + "</td>" +
                            "<td>" + insightsProps.getInsightsStatus() + "</td>" +
                            "<td>" + insightsProps.getClicks() + "</td>" +
                            "<td>" + insightsProps.getImpressions() + "</td>" +
                            "<td>" + insightsProps.getSpend() + "</td>" +
                            "<td>" + creativeProps.getCreativeBody() + "</td>" +
                            "<td>" + creativeProps.getCreativeType() + "</td>" +
                            "<td>" + creativeProps.getImageUrl() + "</td>" +
                            "</tr>";
        }
        return tableHeader + elemAllPropsToHtmlTable + tableEnd;
    }

    public String convertLeadPropsListToHtmlTable(List<LeadProps> leadPropsList) {
        String leadPropsElemToHtmlTable = "";
        String tableHeader =
                "<html>" +
                        "<head>" +
                        "<style>" +
                        "table, th, td {" +
                        "border: " + "1px solid black;" + "}" +
                        "</style>" +
                        "</head>" +
                        "<body>" +
                        "<h2>LeadProps Table</h2>" +
                        "<table style=" + "width:100%" + ">" +
                        "<tr>" +
                        "<th>" + "Ad Id" + "</th>" +
                        "<th>" + "Ad Name" + "</th>" +
                        "<th>" + "Created Time" + "</th>" +
                        "<th>" + "Lead Field Data" + "</th>" +
                        "</tr>";

        String tableEnd =
                "</table>" +
                        "</body>" +
                        "</html>";

        if (leadPropsList != null) {
            for (LeadProps leadProps : leadPropsList) {
                leadPropsElemToHtmlTable +=
                        "<tr>" +
                                "<td>" + leadProps.getAdId() + "</td>" +
                                "<td>" + leadProps.getAdName() + "</td>" +
                                "<td>" + leadProps.getCreatedTime() + "</td>" +
                                "<td>" + leadProps.getFieldData() + "</td>" +
                                "</tr>";
            }
        }
        return tableHeader + leadPropsElemToHtmlTable + tableEnd;
    }
}
