package com.example.facebooksdk.main.service;

import com.example.facebooksdk.entity.*;
import com.example.facebooksdk.entity.repository.AdPropsRepository;
import com.example.facebooksdk.entity.repository.CreativePropsRepository;
import com.example.facebooksdk.entity.repository.InsightsPropsRepository;
import com.example.facebooksdk.entity.repository.LeadPropsRepository;
import com.example.facebooksdk.main.request.FacebookRequestService;
import com.example.facebooksdk.main.utils.Utils;
import com.facebook.ads.sdk.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class DataRequestsDispatcher {
    @Autowired
    private FacebookRequestService facebookRequestService;
    @Autowired
    private Utils utils;
    @Autowired
    private HtmlConverter htmlConverter;
    @Autowired
    private CertainPropsTableGetter propsTableGetter;
    @Autowired
    private PropsToDataBaseInitializer propsToDataBaseInitializer;
    @Autowired
    private AdPropsRepository adPropsRepository;
    @Autowired
    private InsightsPropsRepository insightsPropsRepository;
    @Autowired
    private CreativePropsRepository creativePropsRepository;
    @Autowired
    private LeadPropsRepository leadPropsRepository;

    public String dispatchFrontEndRequest(String adAccountId, String pageId, String parameter, RequestType type) {
        String response = "No data";
        switch (type) {
            case GET_ADACCOUNTS:
                response = processGetAdAccountsDataCommand();
                break;
            case GET_INSIGHTS:
                response = processGetInsightsDataCommand(adAccountId);
                break;
            case GET_ADS:
                response = processGetAdsCommand(adAccountId);
                break;
            case GET_AD_CREATIVE_DATA:
                response = processAdCreativesCommand(adAccountId);
                break;
            case GET_AD_CREATIVE_DATA_BY_ID:
                response = processFormattedAdCreativeCommand(adAccountId, parameter);
                break;
            case GET_ADS_PROPS:
                response = processAdPropsFromDatabaseCommand();
                break;
            case GET_INSIGHTS_PROPS:
                response = processInsightsPropsFromDatabaseCommand();
                break;
            case GET_CREATIVE_PROPS:
                response = processCreativePropsFromDatabaseCommand();
                break;
            case GET_All_PROPS:
                response = processAllPropsFromDataBaseCommand();
                break;
            case GET_CERTAIN_PROPS:
                response = processCertainPropsTableFromDataBaseByTableNameCommand(parameter);
                break;
            case GET_LEAD_PROPS:
                response = processLeadPropsFromDatabaseCommand();
                break;
            case INITIALIZE_ALL_PROPS_TO_DATABASE:
                response = processSaveAllPropsIntoDatabaseCommand(adAccountId);
                break;
            case INITIALIZE_LEAD_PROPS_TO_DATABASE:
                response = processSaveLeadPropsIntoDatabaseCommand(pageId);
                break;

            default:
                System.out.println("Unknown type!");
                break;
        }
        log.warn("Handled request: {}, for adAccount id (if it needs) {}, " +
                "or  page id (if it needs) {}. Prepared response {}", type, adAccountId, pageId, response);
        return response;
    }

    private String processGetAdAccountsDataCommand() {
        APINodeList<AdAccount> adAccountList = facebookRequestService.getAdAccounts();
        if (adAccountList != null) {
            return adAccountList.toString();
        } else {
            return "null data at your list of adaccounts";
        }
    }

    private String processGetInsightsDataCommand(String adAccountId) {
        APINodeList<AdsInsights> adsInsightsList = facebookRequestService.getAllInsightsList(adAccountId);
        if (adsInsightsList != null) {
            return adsInsightsList.toString();
        } else {
            return "null data at your list of insights";
        }
    }

    private String processGetAdsCommand(String adAccountId) {
        APINodeList<Ad> adAPINodeList = facebookRequestService.getAdsDataList(adAccountId);
        if (adAPINodeList != null) {
            return adAPINodeList.toString();
        } else {
            return "null data at your list of ads!";
        }
    }

    private String processAdCreativesCommand(String adAccountId) {
        APINodeList<AdCreative> adCreativeAPINodeList = facebookRequestService.getAllCreativeList(adAccountId);
        if (adCreativeAPINodeList != null) {
            return adCreativeAPINodeList.toString();
        } else {
            return "null data at your list of creatives!";
        }
    }

    private String processFormattedAdCreativeCommand(String adAccountId, String adCreativeId) {
        APINodeList<AdCreative> adCreativeAPINodeList = facebookRequestService.getAllCreativeList(adAccountId);
        AdCreative sortedCreative = utils.getAdCreativeById(adCreativeId, adCreativeAPINodeList);
        return utils.getFormattedDataFromAdCreativeElem(sortedCreative);
    }

    private String processAdPropsFromDatabaseCommand() {
        List<AdProps> adPropsList = adPropsRepository.findAll();
        return htmlConverter.convertAdPropsListToHtmlTable(adPropsList);
    }

    private String processInsightsPropsFromDatabaseCommand() {
        List<InsightsProps> insightsPropsList = insightsPropsRepository.findAll();
        return htmlConverter.convertInsightsPropsListToHtmlTable(insightsPropsList);
    }

    private String processCreativePropsFromDatabaseCommand() {
        List<CreativeProps> creativePropsList = creativePropsRepository.findAll();
        return htmlConverter.convertCreativePropsListToHtmlTable(creativePropsList);
    }

    private String processAllPropsFromDataBaseCommand() {
        List<AdProps> adPropsList = adPropsRepository.findAll();
        List<InsightsProps> insightsPropsList = insightsPropsRepository.findAll();
        List<CreativeProps> creativePropsList = creativePropsRepository.findAll();
        return htmlConverter.convertAllPropsToHtmlTable(
                adPropsList
                , insightsPropsList
                , creativePropsList);
    }

    private String processLeadPropsFromDatabaseCommand() {
        List<LeadProps> leadPropsList = leadPropsRepository.findAll();
        return htmlConverter.convertLeadPropsListToHtmlTable(leadPropsList);
    }

    private String processCertainPropsTableFromDataBaseByTableNameCommand(String requestTable) {
        return propsTableGetter.getCertainPropsTableFromDataBaseByTableName(requestTable);
    }

    private String processSaveAllPropsIntoDatabaseCommand(String adAccountId) {
        if (true == propsToDataBaseInitializer.getInitializedPropsToDataBase(adAccountId)) {
            return "Database was successful initialized by Props entities";
        } else {
            return "Database did not initialize by Props entities";
        }
    }

    private String processSaveLeadPropsIntoDatabaseCommand(String pageId) {
        if (true == propsToDataBaseInitializer.getInitializedLeadPropsToDataBase(pageId)) {
            return "Database was successful initialized by Lead Props entities";
        } else {
            return "Database did not initialize by Lead Props entities";
        }
    }
}






