package com.example.facebooksdk.main.service;

import com.example.facebooksdk.entity.AdAccountOverview;
import com.example.facebooksdk.entity.AdAccountOverviewDateRangeType;
import com.example.facebooksdk.entity.RequestType;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;


public class AdAccountInsightsListData {

    @Autowired
    private AdAccountInsightsFiller filler;

    public List<AdAccountOverview> getFilledEntitiesList(String adAccountId) {
        List<AdAccountOverview> currentInsightsList = new ArrayList<>();

        AdAccountOverview insightsCurrentYesterday = filler.getInitializedAdAccountInsights(
                adAccountId,
                1,   //yesterday
                RequestType.ONE_DAY_RANGE);
        AdAccountOverview insightsPreviousYesterday = filler.getInitializedAdAccountInsights(
                adAccountId,
                2, //before yesterday
                RequestType.ONE_DAY_RANGE);

        AdAccountOverview insightsCurrentLastWeek = filler.getInitializedAdAccountInsights(
                adAccountId,
                7, //last week
                RequestType.CURRENT_RANGE);
        AdAccountOverview insightsPreviousLastWeek = filler.getInitializedAdAccountInsights(
                adAccountId,
                7, //previous last week
                RequestType.PREVIOUS_RANGE);

        AdAccountOverview insightsCurrentLastTwoWeeks = filler.getInitializedAdAccountInsights(
                adAccountId,
                14, //last two weeks
                RequestType.CURRENT_RANGE);
        AdAccountOverview insightsPreviousLastTwoWeeks = filler.getInitializedAdAccountInsights(
                adAccountId,
                14, //previous last two weeks
                RequestType.PREVIOUS_RANGE);

        compareEntitiesByRoas(insightsCurrentYesterday, insightsPreviousYesterday);
        compareEntitiesByRoas(insightsCurrentLastWeek, insightsPreviousLastWeek);
        compareEntitiesByRoas(insightsCurrentLastTwoWeeks, insightsPreviousLastTwoWeeks);

        insightsCurrentYesterday.setAdAccountOverviewDateRangeType(AdAccountOverviewDateRangeType.YESTERDAY);
        insightsCurrentLastWeek.setAdAccountOverviewDateRangeType(AdAccountOverviewDateRangeType.LAST_WEEK);
        insightsCurrentLastTwoWeeks.setAdAccountOverviewDateRangeType(AdAccountOverviewDateRangeType.LAST_TWO_WEEKS);

        currentInsightsList.add(insightsCurrentYesterday);
        currentInsightsList.add(insightsCurrentLastWeek);
        currentInsightsList.add(insightsCurrentLastTwoWeeks);

        return currentInsightsList;
    }

    private void compareEntitiesByRoas(AdAccountOverview insightsCurrentPeriod, AdAccountOverview insightsPreviousPeriod) {

        if (insightsCurrentPeriod.getPurchaseRoas() > insightsPreviousPeriod.getPurchaseRoas()
                && insightsPreviousPeriod.getPurchaseRoas() != 0.0) {
            insightsCurrentPeriod.setPurchaseRoasStatus("up_rise");
            int percentage = (int)
                    (((insightsCurrentPeriod.getPurchaseRoas() / insightsPreviousPeriod.getPurchaseRoas()) * 100) - 100);
            insightsCurrentPeriod.setPurchaseRoasPercentage(percentage);
        } else if (insightsCurrentPeriod.getPurchaseRoas() > insightsPreviousPeriod.getPurchaseRoas()
                && insightsPreviousPeriod.getPurchaseRoas() == 0.0) {
            insightsCurrentPeriod.setPurchaseRoasStatus("up_rise");
            int percentage = (int) ((insightsCurrentPeriod.getPurchaseRoas() * 100) - 100);
            insightsCurrentPeriod.setPurchaseRoasPercentage(percentage);
        } else if (insightsCurrentPeriod.getPurchaseRoas() < insightsPreviousPeriod.getPurchaseRoas()
                && insightsPreviousPeriod.getPurchaseRoas() != 0.0) {
            insightsCurrentPeriod.setPurchaseRoasStatus("down_drop");
            int percentage = (int)
                    (100 - ((insightsCurrentPeriod.getPurchaseRoas() / insightsPreviousPeriod.getPurchaseRoas()) * 100));
            insightsCurrentPeriod.setPurchaseRoasPercentage(percentage);

        } else if (insightsCurrentPeriod.getPurchaseRoas() < insightsPreviousPeriod.getPurchaseRoas()
                && insightsCurrentPeriod.getPurchaseRoas() == 0.0) {
            insightsCurrentPeriod.setPurchaseRoasStatus("down_drop");
            int percentage = 100;   //cause if we have insightsCurrentPeriod = 0.0, then our percentage always will be 100%
            insightsCurrentPeriod.setPurchaseRoasPercentage(percentage);
        } else {
            insightsCurrentPeriod.setPurchaseRoasStatus("none");
            insightsCurrentPeriod.setPurchaseRoasPercentage(0);
        }
    }
}
