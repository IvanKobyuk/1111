package com.example.facebooksdk.main.service;

import com.example.facebooksdk.entity.AdProps;
import com.example.facebooksdk.entity.CreativeProps;
import com.example.facebooksdk.entity.InsightsProps;
import com.example.facebooksdk.entity.LeadProps;
import com.example.facebooksdk.entity.repository.AdPropsRepository;
import com.example.facebooksdk.entity.repository.CreativePropsRepository;
import com.example.facebooksdk.entity.repository.InsightsPropsRepository;
import com.example.facebooksdk.entity.repository.LeadPropsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class PropsToDataBaseInitializer {

    @Autowired
    private AdPropsFiller adPropsFiller;
    @Autowired
    private InsightsPropsFiller insightsPropsFiller;
    @Autowired
    private CreativePropsFiller creativePropsFiller;
    @Autowired
    private LeadPropsFiller leadPropsFiller;
    @Autowired
    private AdPropsRepository adPropsRepository;
    @Autowired
    private InsightsPropsRepository insightsPropsRepository;
    @Autowired
    private CreativePropsRepository creativePropsRepository;
    @Autowired
    private LeadPropsRepository leadPropsRepository;

    public boolean getInitializedPropsToDataBase(String adAccount) {
        List<AdProps> adPropsList = adPropsFiller.getInitializedAdProps(adAccount);
        List<InsightsProps> insightsPropsList = insightsPropsFiller.getInitializedInsightsProps(adAccount);
        List<CreativeProps> creativePropsList = creativePropsFiller.getInitializedCreativeProps(adAccount);
        if (adPropsList != null & insightsPropsList != null & creativePropsList != null) {
            adPropsRepository.saveAll(adPropsList);
            insightsPropsRepository.saveAll(insightsPropsList);
            creativePropsRepository.saveAll(creativePropsList);
            return true;
        } else {
            log.warn("List of Props does not have any elements. {} {} {}"
                    , adPropsList
                    , insightsPropsList
                    , creativePropsList);
            return false;
        }
    }

    public boolean getInitializedLeadPropsToDataBase(String pageId) {
        List<LeadProps> leadPropsList = leadPropsFiller.getInitializedLeadProps(pageId);
        log.warn("Received leadPropsList before initialize to DB consists. {} ", leadPropsList);

        if (leadPropsList != null) {
            leadPropsRepository.saveAll(leadPropsList);
            return true;
        } else {
            log.warn("List of lead Props does not have any elements.  {}", leadPropsList);
            return false;
        }
    }
}
