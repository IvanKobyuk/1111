package com.example.facebooksdk.main.service;

import com.example.facebooksdk.entity.AdProps;
import com.example.facebooksdk.main.request.FacebookRequestService;
import com.facebook.ads.sdk.Ad;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class AdPropsFiller {
    @Autowired
    private FacebookRequestService requestService;

    public List<AdProps> getInitializedAdProps(String adAccount) {
        List<Ad> adList = requestService.getAdList(adAccount);
        List<AdProps> adPropsList = new ArrayList<>();
//        long idAdProps = 1;

        for (Ad ad : adList) {
            AdProps adProps = initializeEntity(ad);
//            adProps.setId(idAdProps);
            adPropsList.add(adProps);
//            idAdProps++;
        }
        return adPropsList;
    }

    private AdProps initializeEntity(Ad ad) {
        AdProps adProps = new AdProps();
        adProps.setAdId(ad.getFieldId());
        adProps.setAdName(ad.getFieldName());
        return adProps;
    }
}
