package com.example.facebooksdk.main.service;

import com.example.facebooksdk.entity.CreativeProps;
import com.example.facebooksdk.main.request.FacebookRequestService;
import com.facebook.ads.sdk.Ad;
import com.facebook.ads.sdk.AdCreative;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class CreativeFiller {

    @Autowired
    private FacebookRequestService requestService;

    public List<CreativeProps> getInitializedCreativeProps(String adAccount) {
        List<Ad> adList = requestService.getAdList(adAccount);
        List<CreativeProps> creativePropsList = new ArrayList<>();

        for (Ad ad : adList) {
            String adId = ad.getFieldId();
            AdCreative creativeByAdId = requestService.getAdCreativeByAdId(adId);

            if (creativeByAdId != null) {
                if (creativeByAdId.getFieldImageUrl() != null & creativeByAdId.getFieldBody() != null) {
                    CreativeProps creativeProps = initializeEntity(creativeByAdId, ad);
                    creativePropsList.add(creativeProps);
                } else {
                    CreativeProps creativeProps = initializeEntityWithSomeNullFields(creativeByAdId, ad);
                    creativePropsList.add(creativeProps);
                }
            } else {
                CreativeProps creativeProps = initializeEntityWithAllNullData(ad);
                creativePropsList.add(creativeProps);
            }
        }
        return creativePropsList;
    }

    private CreativeProps initializeEntity(AdCreative adCreative, Ad ad) {
        CreativeProps creativeProps = new CreativeProps();
        creativeProps.setAdId(ad.getFieldId());
        creativeProps.setAdName(ad.getFieldName());
        creativeProps.setCreativeBody(adCreative.getFieldBody());
        creativeProps.setImageUrl(adCreative.getFieldImageUrl());
        creativeProps.setCreativeType(adCreative.getFieldObjectType().toString());
        return creativeProps;
    }

    private CreativeProps initializeEntityWithAllNullData(Ad ad) {
        CreativeProps creativeProps = new CreativeProps();
        creativeProps.setAdId(ad.getFieldId());
        creativeProps.setAdName(ad.getFieldName());
        creativeProps.setCreativeBody("no data");
        creativeProps.setImageUrl("no data");
        creativeProps.setCreativeType("no data");
        return creativeProps;
    }

    private CreativeProps initializeEntityWithSomeNullFields(AdCreative adCreative, Ad ad) {
        CreativeProps creativeProps = new CreativeProps();
        creativeProps.setAdId(ad.getFieldId());
        creativeProps.setAdName(ad.getFieldName());
        creativeProps.setCreativeBody("no data");
        creativeProps.setImageUrl("no data");
        creativeProps.setCreativeType(adCreative.getFieldObjectType().toString());
        return creativeProps;
    }

}
