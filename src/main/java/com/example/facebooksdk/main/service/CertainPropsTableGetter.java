package com.example.facebooksdk.main.service;

import com.example.facebooksdk.entity.AdProps;
import com.example.facebooksdk.entity.CreativeProps;
import com.example.facebooksdk.entity.InsightsProps;
import com.example.facebooksdk.entity.RequestType;
import com.example.facebooksdk.entity.repository.AdPropsRepository;
import com.example.facebooksdk.entity.repository.CreativePropsRepository;
import com.example.facebooksdk.entity.repository.InsightsPropsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class CertainPropsTableGetter {
    @Autowired
    private HtmlConverter htmlConverter;
    @Autowired
    private AdPropsRepository adPropsRepository;
    @Autowired
    private InsightsPropsRepository insightsPropsRepository;
    @Autowired
    private CreativePropsRepository creativePropsRepository;

    public String getCertainPropsTableFromDataBaseByTableName(String requestTable) {
        String responseForFront = "Wrong table's name!";
        RequestType requestType = RequestType.valueOf(requestTable.toUpperCase());
        List<AdProps> adPropsList = adPropsRepository.findAll();
        List<InsightsProps> insightsPropsList = insightsPropsRepository.findAll();
        List<CreativeProps> creativePropsList = creativePropsRepository.findAll();
        log.info("Received requestType {}", requestType);
        switch (requestType) {
            case ADS:
                responseForFront = htmlConverter.convertAdPropsListToHtmlTable(adPropsList);
                break;
            case INSIGHTS:
                responseForFront = htmlConverter.convertInsightsPropsListToHtmlTable(insightsPropsList);
                break;
            case CREATIVES:
                responseForFront = htmlConverter.convertCreativePropsListToHtmlTable(creativePropsList);
                break;
            case ALL:
                responseForFront = htmlConverter.convertAllPropsToHtmlTable(
                        adPropsList
                        , insightsPropsList
                        , creativePropsList);
                break;

            default:
                System.out.println("Unknown request case!");
                break;
        }
        log.warn("Handled request : {}. Prepared response {}", requestTable, responseForFront);
        return responseForFront;
    }
}
