package com.example.facebooksdk.main.service;

import com.example.facebooksdk.entity.InsightsProps;
import com.example.facebooksdk.main.request.FacebookRequestService;
import com.facebook.ads.sdk.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class InsightsPropsFiller {
    @Autowired
    private FacebookRequestService requestService;

    public List<InsightsProps> getInitializedInsightsProps(String adAccountId) {
        List<InsightsProps> insightsPropsList = new ArrayList<>();
        List<Ad> adList = requestService.getAdList(adAccountId);

        for (Ad ad : adList) {
            String adFieldId = ad.getFieldId();
            AdsInsights insightsByAdId = requestService.getAdsInsightsByAdId(adFieldId);
            if (insightsByAdId != null) {
                InsightsProps insightsProps
                        = initializeEntity(insightsByAdId, ad);
                insightsPropsList.add(insightsProps);
            } else {
                InsightsProps insightsProps
                        = initializeEntityWithAllNullData(ad);
                insightsPropsList.add(insightsProps);
            }
        }
        return insightsPropsList;
    }

    private InsightsProps initializeEntity(AdsInsights insights, Ad ad) {
        InsightsProps insightsPropsEntity = new InsightsProps();
        insightsPropsEntity.setAdAccountName(insights.getFieldAccountName());
        insightsPropsEntity.setCampaignName(insights.getFieldCampaignName());
        insightsPropsEntity.setAdName(insights.getFieldAdName());
        insightsPropsEntity.setAdId(ad.getFieldId());
        insightsPropsEntity.setClicks(insights.getFieldClicks());
        insightsPropsEntity.setImpressions(insights.getFieldImpressions());
        insightsPropsEntity.setSpend(insights.getFieldSpend());
        insightsPropsEntity.setInsightsStatus("Activated");
        return insightsPropsEntity;
    }

    private InsightsProps initializeEntityWithAllNullData(Ad ad) {
        InsightsProps insightsPropsEntity = new InsightsProps();
        insightsPropsEntity.setAdAccountName(ad.getFieldAccountId());
        insightsPropsEntity.setCampaignName(ad.getFieldCampaignId());
        insightsPropsEntity.setAdName(ad.getFieldName());
        insightsPropsEntity.setAdId(ad.getFieldId());
        insightsPropsEntity.setClicks("no data");
        insightsPropsEntity.setImpressions("no data");
        insightsPropsEntity.setSpend("no data");
        insightsPropsEntity.setInsightsStatus("Inactivated");
        return insightsPropsEntity;
    }
}
