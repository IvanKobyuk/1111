package com.example.facebooksdk.main.service;

import com.example.facebooksdk.entity.LeadProps;
import com.example.facebooksdk.main.request.FacebookRequestService;
import com.example.facebooksdk.main.utils.Utils;
import com.facebook.ads.sdk.Lead;
import com.facebook.ads.sdk.UserLeadGenFieldData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class LeadPropsFiller {

    @Autowired
    private FacebookRequestService facebookRequestService;
    @Autowired
    private Utils utils;

    public List<LeadProps> getInitializedLeadProps(String pageId) {
        List<Lead> leadList = facebookRequestService.getAdLeads(pageId);
        List<LeadProps> leadPropsList = new ArrayList<>();

        for (Lead lead : leadList) {
            if (lead.getFieldAdId() != null) {
                LeadProps leadProps = initializeEntity(lead);
                leadPropsList.add(leadProps);
            }
        }
        return leadPropsList;
    }

    private LeadProps initializeEntity(Lead lead) {
        LeadProps leadProps = new LeadProps();
        leadProps.setAdId(lead.getFieldAdId());
        leadProps.setAdName(lead.getFieldAdName());
        leadProps.setCreatedTime(utils.getFormattedCreatedTime(lead.getFieldCreatedTime()));
        try {
            leadProps.setFieldData(utils.getParsedFieldDataFromLead(lead));
        } catch (JSONException e) {
            log.warn("Error retrieving data from lead field field_data {}", leadProps);

            e.printStackTrace();
        }
        return leadProps;
    }

    private String getDataFromFieldData(Lead lead, String adId) {
        List<UserLeadGenFieldData> dataFieldList = lead.getFieldFieldData();
        String emailField = "";

        for (UserLeadGenFieldData fieldData : dataFieldList) {
            if (lead.getFieldAdId().equals(adId)) {
                List<String> fieldValues = fieldData.getFieldValues();
                String emailValue = "@";
                for (String value : fieldValues) {
                    log.warn("value from field data, {} ", value);
                    if (value.contains(emailValue)) {
                        emailField = value;
                    }
                }
            }
        }
        return emailField;
    }
}
