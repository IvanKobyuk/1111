package com.example.facebooksdk.main.utils;

import com.example.facebooksdk.entity.CreativeProps;
import com.example.facebooksdk.entity.InsightsProps;
import com.facebook.ads.sdk.AdCreative;
import com.facebook.ads.sdk.Lead;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class Utils {

    public String getFormattedOneDateByDaysCount(int dayCount) {
        LocalDate today = LocalDate.now();
        LocalDate oneDate = today.minusDays(dayCount);
        return "{since:'" + oneDate.toString() + "',until:'" + oneDate.toString() + "'}";  //get time range for one day
    }

    public String getFormattedCurrentTimeRangeByDaysCount(int dayCount) {
        LocalDate until = LocalDate.now().minusDays(1); //exclude today`s data
        LocalDate since = until.minusDays(dayCount - 1);
        return "{since:'" + since.toString() + "',until:'" + until.toString() + "'}";
    }

    public String getFormattedPreviousTimeRangeByDaysCount(int dayCount) {
        LocalDate until = LocalDate.now().minusDays(dayCount + 1); //exclude today`s data
        LocalDate since = until.minusDays(dayCount - 1);
        return "{since:'" + since.toString() + "',until:'" + until.toString() + "'}";
    }

    public String getFormattedTimeRange(int monthCount) {
        LocalDate until = LocalDate.now();
        LocalDate since = until.minusMonths(monthCount);
        return "{since:'" + since.toString() + "',until:'" + until.toString() + "'}";
    }

    public String getFormattedDataFromAdCreativeElem(AdCreative adCreativeElem) {
        return "AdCreative info from your AdAccount are:" + "<br>"
                + "id of ad account = " + adCreativeElem.getFieldAccountId() + "<br>"
                + "id of ad creative = " + adCreativeElem.getId() + "<br>"
                + "status field = " + adCreativeElem.getFieldStatus() + "<br>"
                + "name field = " + adCreativeElem.getFieldName() + "<br>"
                + "url of image for appropriate  ad creative = " + "<br>" + adCreativeElem.getFieldImageUrl();
    }

    public InsightsProps getInsightsPropsElemFromListByAdId(String adId, List<InsightsProps> insightsPropsList) {
        InsightsProps sortedInsightsPropsByAdId = null;
        for (InsightsProps elem : insightsPropsList) {
            if (adId.equals(elem.getAdId())) {
                sortedInsightsPropsByAdId = elem;
            }
        }
        return sortedInsightsPropsByAdId;
    }

    public CreativeProps getCreativePropsElemFromListByAdId(String adId, List<CreativeProps> creativePropsList) {
        CreativeProps sortedCreativePropsByAdId = null;
        for (CreativeProps elem : creativePropsList) {
            if (adId.equals(elem.getAdId())) {
                sortedCreativePropsByAdId = elem;
            }
        }
        return sortedCreativePropsByAdId;
    }

    public AdCreative getAdCreativeById(String adCreativeId, List<AdCreative> adCreativeList) {
        AdCreative sortedCreative = null;
        for (AdCreative elem : adCreativeList) {
            if (adCreativeId.equals(elem.getId())) {
                sortedCreative = elem;
            }
        }
        return sortedCreative;
    }

    /**
     * method getFormattedCreatedTime() is actual to cases,
     * when it needs to convert local parameter
     * from date`s format such as "yyyy-MM-dd'T'HH:mm:ss"
     * to output format "dd-MM-yyyy HH:mm:ss"
     */
    public String getFormattedCreatedTime(String inputTime) {
        SimpleDateFormat inputTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat outputTimeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = null;
        try {
            date = inputTimeFormat.parse(inputTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputTimeFormat.format(date);
    }

    public String getParsedFieldDataFromLead(Lead lead) throws JSONException {
        JSONArray jsonArrayFieldDataFromLead = null;
        try {
            jsonArrayFieldDataFromLead = new JSONArray(lead.getFieldFieldData().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        log.warn("Received field data from lead {} ", jsonArrayFieldDataFromLead);

        List<String> fieldNamesList = new ArrayList<>();
        List<String> fieldValuesList = new ArrayList<>();
        List<String> allFieldDataList = new ArrayList<>();

        if (jsonArrayFieldDataFromLead != null) {
            for (int i = 0; i < jsonArrayFieldDataFromLead.length(); i++) {
                JSONObject nameDataJson = new JSONObject();
                try {
                    nameDataJson = jsonArrayFieldDataFromLead.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String fieldName = nameDataJson.getString("name");
                JSONArray jsonArrayDataValue = nameDataJson.getJSONArray("values");
                for (int j = 0; j < jsonArrayDataValue.length(); j++) {
                    String valueField = jsonArrayDataValue.get(j).toString();
                    fieldValuesList.add(valueField);
                }
                fieldNamesList.add(fieldName);
            }
        }

        String mergedFields = "";
        for (int i = 0; i < fieldNamesList.size(); i++) {
            for (int j = 0; j < fieldValuesList.size(); j++) {
                if (i == j) {
                    mergedFields = fieldNamesList.get(i) + ": " + fieldValuesList.get(j);
                    allFieldDataList.add(mergedFields);
                }
            }
        }
        log.warn("Received  lead data from 'field_data' {}", allFieldDataList);

        return allFieldDataList.toString();
    }
}
