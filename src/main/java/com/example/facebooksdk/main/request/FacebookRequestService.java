package com.example.facebooksdk.main.request;

import com.example.facebooksdk.entity.RequestType;
import com.example.facebooksdk.main.utils.Utils;
import com.facebook.ads.sdk.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class FacebookRequestService {
    @Value("${facebook.api.app.secret}")
    private String appSecret;
    @Value("${facebook.api.access.token}")
    private String accessToken;
    @Value("${facebook.api.user.id}")
    private String userId;
    @Autowired
    private Utils utilise;

    public APINodeList<AdAccount> getAdAccounts(String userId) {
        APIContext apiContext = new APIContext(accessToken, appSecret).enableDebug(false);
        User.APIRequestGetAdAccounts adAccountsRequest = null;
        APINodeList<AdAccount> adAccountsList = null;
        try {
            adAccountsRequest = new User(userId, apiContext)
                    .getAdAccounts()
                    .requestNameField()
                    .requestAccountIdField();
            adAccountsList = adAccountsRequest
                    .execute();
        } catch (APIException e) {
            e.printStackTrace();
        }
        return adAccountsList;
    }

    public APINodeList<AdsInsights> getAdAccountInsights(String adAccountId, int dayCount, RequestType requestType) {
        APIContext apiContext = new APIContext(accessToken, appSecret).enableDebug(false);
        AdAccount.APIRequestGetInsights adAccountInsightsRequest = null;
        String timeRange = getTimeRangeByType(requestType, dayCount);
        APINodeList<AdsInsights> adsInsightsResponse = null;
        try {
            adAccountInsightsRequest = new AdAccount(adAccountId, apiContext)
                    .getInsights()
                    .setTimeRange(timeRange)
                    .requestField("spend")
                    .requestField("purchase_roas")
                    .requestField("action_values")
                    .requestField("cost_per_action_type")
                    .requestField("actions");
            adsInsightsResponse = adAccountInsightsRequest
                    .execute();
        } catch (APIException e) {
            e.printStackTrace();
        }
        return adsInsightsResponse;
    }

    public APINodeList<AdsInsights> getAdAccountNameAndCurrency(String adAccountId) {
        APIContext apiContext = new APIContext(accessToken, appSecret).enableDebug(false);
        AdAccount.APIRequestGetInsights adAccountInsightsRequest = null;
        APINodeList<AdsInsights> adsInsightsResponse = null;
        try {
            adAccountInsightsRequest = new AdAccount(adAccountId, apiContext)
                    .getInsights()
                    .requestField("account_name")
                    .requestField("account_currency");
            adsInsightsResponse = adAccountInsightsRequest
                    .execute();
        } catch (APIException e) {
            e.printStackTrace();
        }
        return adsInsightsResponse;
    }

    private String getTimeRangeByType(RequestType type, int dayCount) {
        String timeRange = "No data";
        switch (type) {
            case ONE_DAY_RANGE:
                timeRange = utilise.getFormattedOneDateByDaysCount(dayCount);
                break;
            case CURRENT_RANGE:
                timeRange = utilise.getFormattedCurrentTimeRangeByDaysCount(dayCount);
                break;
            case PREVIOUS_RANGE:
                timeRange = utilise.getFormattedPreviousTimeRangeByDaysCount(dayCount);
                break;
            default:
                System.out.println("Unknown type!");
                break;
        }
        return timeRange;
    }

    public List<Ad> getAdList(String adAccount) {
        APIContext apiContext = new APIContext(accessToken, appSecret).enableDebug(false);
        APINodeList<Ad> adAPINodeList = null;
        List<Ad> adList = new ArrayList<>();

        try {
            adAPINodeList = new AdAccount(adAccount, apiContext)
                    .getAds()
                    .requestIdField()
                    .requestNameField()
                    .requestCampaignIdField()
                    .requestAccountIdField()
                    .requestStatusField()
                    .execute();
            adList.addAll(adAPINodeList);
        } catch (APIException e) {
            e.printStackTrace();
        }
        return adList;
    }

    public AdsInsights getAdsInsightsByAdId(String adId) {
        APIContext apiContext = new APIContext(accessToken, appSecret).enableDebug(false);
        Ad.APIRequestGetInsights adsInsightsRequest = null;
        String timeRange = utilise.getFormattedTimeRange(3);
        APINodeList<AdsInsights> adsInsightsResponse = null;
        AdsInsights adsInsights = null;

        try {
            adsInsightsRequest = new Ad(adId, apiContext)
                    .getInsights()
                    .setTimeRange(timeRange)
                    .requestField("account_name")
                    .requestField("campaign_name")
                    .requestField("ad_name")
                    .requestField("clicks")
                    .requestField("impressions")
                    .requestField("spend");
            adsInsightsResponse = adsInsightsRequest.execute();

            if (adsInsightsResponse != null) {
                for (AdsInsights insights : adsInsightsResponse) {
                    adsInsights = insights;
                }
            }
        } catch (APIException e) {
            e.printStackTrace();
        }
        return adsInsights;
    }

    public AdCreative getAdCreativeByAdId(String adId) {
        APIContext apiContext = new APIContext(accessToken, appSecret).enableDebug(false);
        APINodeList<AdCreative> adCreativeAPINodeList;
        AdCreative adCreative = null;

        try {
            adCreativeAPINodeList = new Ad(adId, apiContext)
                    .getAdCreatives()
                    .requestImageUrlField()
                    .requestThumbnailUrlField()
                    .requestBodyField()
                    .requestObjectTypeField()
                    .execute();
            if (adCreativeAPINodeList != null) {
                for (AdCreative creative : adCreativeAPINodeList) {
                    adCreative = creative;
                }
            }
        } catch (APIException e) {
            e.printStackTrace();
        }
        return adCreative;
    }

    public APINodeList<AdAccount> getAdAccounts() {
        APIContext apiContext = new APIContext(accessToken, appSecret).enableDebug(false);
        User.APIRequestGetAdAccounts adAccountsRequest = new User(userId, apiContext)
                .getAdAccounts()
                .requestNameField()
                .requestAccountIdField()
                .requestCreatedTimeField();
        APINodeList<AdAccount> adAccounts = null;

        try {
            adAccounts = adAccountsRequest.execute();
        } catch (APIException e) {
            e.printStackTrace();
        }
        return adAccounts;
    }

    public APINodeList<AdCreative> getAllCreativeList(String adAccountId) {
        APIContext apiContext = new APIContext(accessToken, appSecret).enableDebug(false);
        AdAccount adAccount = new AdAccount(adAccountId, apiContext);
        APINodeList<AdCreative> adCreativesList = null;

        try {
            adCreativesList = adAccount
                    .getAdCreatives()
                    .requestAccountIdField()
                    .requestStatusField()
                    .requestIdField()
                    .requestNameField()
                    .requestImageUrlField()
                    .execute();
        } catch (APIException e) {
            e.printStackTrace();
        }
        return adCreativesList;
    }

    public APINodeList<AdsInsights> getAllInsightsList(String adAccountId) {
        APIContext apiContext = new APIContext(accessToken, appSecret).enableDebug(true);
        AdAccount adAccount = new AdAccount(adAccountId, apiContext);
        AdAccount.APIRequestGetInsights adsInsightsRequest = null;
        String timeRange = utilise.getFormattedTimeRange(3);
        APINodeList<AdsInsights> adsInsightsResponse = null;

        try {
            adsInsightsRequest = adAccount
                    .getInsights()
                    .setTimeRange(timeRange)
                    .requestField("account_name")
                    .requestField("campaign_name")
                    .requestField("ad_name")
                    .requestField("clicks")
                    .requestField("impressions")
                    .requestField("spend")
                    .requestField("unique_actions")
                    .requestField("ad_id");
            adsInsightsResponse = adsInsightsRequest.execute();
        } catch (APIException e) {
            e.printStackTrace();
        }
        if (adsInsightsResponse != null) {
            log.info("Received response for insights {}", adsInsightsResponse.getRawResponse());
        }
        return adsInsightsResponse;
    }

    public APINodeList<Ad> getAdsDataList(String adAccountId) {
        APIContext apiContext = new APIContext(accessToken, appSecret).enableDebug(false);
        AdAccount adAccount = new AdAccount(adAccountId, apiContext);
        APINodeList<Ad> adsList = null;

        try {
            adsList = adAccount
                    .getAds()
                    .requestAccountIdField()
                    .requestCampaignField()
                    .requestNameField()
                    .requestCreatedTimeField()
                    .execute();
        } catch (APIException e) {
            e.printStackTrace();
        }
        return adsList;
    }

    public List<Lead> getAdLeads(String pageId) {
        APIContext apiContext = new APIContext(accessToken, appSecret).enableDebug(false);
        User user = new User(userId, apiContext);

        APINodeList<Page> pages = null;
        try {
            pages = user
                    .getAccounts()
                    .execute();
        } catch (APIException e) {
            e.printStackTrace();
        }

        String fieldAccessToken = "";

        for (Page page : pages) {
            if (page.getFieldId().equals(pageId)) {
                fieldAccessToken = page.getFieldAccessToken();
            }
        }

        APIContext apiContextForPage = new APIContext(fieldAccessToken, appSecret);

        Page page = new Page(pageId, apiContextForPage);
        APINodeList<LeadgenForm> leadgenForms = null;
        try {
            leadgenForms = page
                    .getLeadGenForms()
                    .execute();
        } catch (APIException e) {
            e.printStackTrace();
        }

        APINodeList<Lead> leadAPINodeListRequest = null;
        List<Lead> leadList = new ArrayList<>();
        for (LeadgenForm leadgenForm : leadgenForms) {

            try {
                leadAPINodeListRequest = leadgenForm
                        .getLeads()
                        .requestAdIdField()
                        .requestAdNameField()
                        .requestCreatedTimeField()
                        .requestField("field_data")
                        .execute();
            } catch (APIException e) {
                e.printStackTrace();
            }

            if (leadAPINodeListRequest != null) {
                leadList.addAll(leadAPINodeListRequest);
            }
        }

        return leadList;
    }
}
