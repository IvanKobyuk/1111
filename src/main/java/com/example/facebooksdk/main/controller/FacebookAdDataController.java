package com.example.facebooksdk.main.controller;

import com.example.facebooksdk.main.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static com.example.facebooksdk.entity.RequestType.*;

@RestController
@Slf4j
public class FacebookAdDataController {

    @Autowired
    private DataRequestsDispatcher dataRequestsDispatcher;

    @GetMapping("/adaccounts")
    public String getAdAccount() {
        String defaultResponse = "List of your ";
        String nonDefaultResponse = dataRequestsDispatcher.dispatchFrontEndRequest(
                null,
                null,
                null,
                GET_ADACCOUNTS);
        String responseForFront = defaultResponse + "" + "\n" + nonDefaultResponse;
        log.info("Request handled. Prepared response: {}", responseForFront);
        return responseForFront;
    }

    @GetMapping("/ads/all")
    public ResponseEntity<?> getAds(@Valid @RequestParam("aaid") @NotNull String adAccountId) {
        log.info("Received ads request: {} processing...", adAccountId);
        String adAccountIdResponse = dataRequestsDispatcher.dispatchFrontEndRequest(adAccountId,
                null,
                null,
                GET_ADS);
        return new ResponseEntity<>(adAccountIdResponse, HttpStatus.OK);
    }

    @GetMapping("/creatives/all")
    public ResponseEntity<?> getAdCreatives(@Valid @RequestParam("aaid") @NotNull String adAccountId) {
        log.info("Received adCreatives request: {} processing...", adAccountId);
        String response = dataRequestsDispatcher.dispatchFrontEndRequest(adAccountId,
                null,
                null,
                GET_AD_CREATIVE_DATA);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/insights/all")
    public ResponseEntity<?> getAdInsights(@Valid @RequestParam("aaid") @NotNull String adAccountId) {
        log.info("Received adInsights request: {} processing...", adAccountId);
        String response = dataRequestsDispatcher.dispatchFrontEndRequest(adAccountId,
                null,
                null,
                GET_INSIGHTS);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/creatives/id")
    public ResponseEntity<?> getAdCreative(@Valid @RequestParam("aaid") @NotNull String adAccountId,
                                           @Valid @RequestParam("acid") @NotNull String creativeId) {
        log.info("Received adCreatives request by two parameters: {} {}  processing...", adAccountId, creativeId);
        String response = dataRequestsDispatcher.dispatchFrontEndRequest(adAccountId,
                null,
                creativeId,
                GET_AD_CREATIVE_DATA_BY_ID);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/props/initialize/to/database")
    public ResponseEntity<?> getInitializedPropsIntoDatabase(@Valid @RequestParam("aaid") @NotNull String adAccountId) {
        log.info("Received props save to DB request: {} processing...", adAccountId);
        String response = dataRequestsDispatcher.dispatchFrontEndRequest(adAccountId,
                null,
                null,
                INITIALIZE_ALL_PROPS_TO_DATABASE);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/leadprops/initialize/to/database")
    public ResponseEntity<?> getInitializedLeadPropsIntoDatabase(@Valid @RequestParam("pid") @NotNull String pageId) {
        log.info("Received lead props save to DB request: {} processing...", pageId);
        String response = dataRequestsDispatcher.dispatchFrontEndRequest(null,
                pageId,
                null,
                INITIALIZE_LEAD_PROPS_TO_DATABASE);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * first necessary step before use controller,
     * that get any props entities from database is
     * to launch  controller (find necessary controller over this comment), that initialize props to database
     */

    @GetMapping("database/props/ads")
    public ResponseEntity<?> getAdPropsEntityFromDatabase() {
        String response = dataRequestsDispatcher.dispatchFrontEndRequest(null,
                null,
                null,
                GET_ADS_PROPS);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/database/props/insights")
    public ResponseEntity<?> getInsightsPropsEntityFromDatabase() {
        String response = dataRequestsDispatcher.dispatchFrontEndRequest(
                null,
                null,
                null,
                GET_INSIGHTS_PROPS);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/database/props/creatives")
    public ResponseEntity<?> getCreativePropsEntityFromDatabase() {
        String response = dataRequestsDispatcher.dispatchFrontEndRequest(
                null,
                null,
                null,
                GET_CREATIVE_PROPS);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("database/props/ads+insights+creatives")
    public ResponseEntity<?> getAllPropsFromDataBase() {
        String response = dataRequestsDispatcher.dispatchFrontEndRequest(
                null,
                null,
                null,
                GET_All_PROPS);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("database/props")
    public ResponseEntity<?> getCertainPropsFromDataBase(@Valid @RequestParam("table") @NotNull String requestTable) {
        log.info("Received table of props request by table name: {} processing...", requestTable);
        String response = dataRequestsDispatcher.dispatchFrontEndRequest(
                null,
                null,
                requestTable,
                GET_CERTAIN_PROPS);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/database/props/leads")
    public ResponseEntity<?> getLeadPropsEntityFromDatabase() {
        String response = dataRequestsDispatcher.dispatchFrontEndRequest(
                null,
                null,
                null,
                GET_LEAD_PROPS);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}