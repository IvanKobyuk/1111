package com.example.facebooksdk.main.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.annotation.PostConstruct;

@Configuration
@ComponentScan
@PropertySource(value = "classpath:const-data.properties")
@PropertySource(value = "classpath:db.properties")
@EnableJpaRepositories(basePackages = "com.example.facebooksdk.entity.repository")
@EntityScan("com.example.facebooksdk.entity")
public class MyApplicationContextConfiguration {
    @Value("${spring.datasource.url}")
    private String dataUrl;
    @Value("${spring.datasource.username}")
    private String username;


    @PostConstruct
    public void init() {
        System.out.println("### " + dataUrl + " | " + username);
    }
}
