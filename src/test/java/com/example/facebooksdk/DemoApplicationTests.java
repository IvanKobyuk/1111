package com.example.facebooksdk;

import com.facebook.ads.sdk.*;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

class DemoApplicationTests {

    private static final String appSecret = "36f2264278bfb1cc91941f5d6ad0f395";
    private static final String accessToken = "EAAE9C3mLiFABAEv9NVRwGZAHqHhtZCXcU5K1TfsWiRWSKw2q0ZC8BfDsZCFF0z4fZAmkZAocG8qVPJWZCeQTRqSwfcBeZCJNZCAZBziYOQzPgnXIgrZBiZAUtPOCs06Eor9KWxxvuVMks0GOYv2HEqEAilyc4l8Mn3SYu0RlMtZAgd1NYPoXyIPe3OoWcTcuDrZCiEaZA0ZD";

    @Test
    @Ignore
    void contextLoads() throws APIException {

//        String datesRange = getFormattedTimerange(6);
//        System.out.println(datesRange);

        getCampaignName();
    }


    private void getCampaignName() throws APIException {

        APIContext apiContext = new APIContext(accessToken, appSecret).enableDebug(false);

        APINodeList<Ad> ads = new AdAccount("290196767803714", apiContext).getAds().execute();
        System.out.println(ads);

        for (Ad ad : ads) {
            APINodeList<AdsInsights> insights = ad.getInsights().requestField("campaign_name").execute();
            insights.forEach(insight -> System.out.println(insight.getFieldCampaignName()));
        }
    }

    private String getFormattedTimerange(int monthCount) {
        LocalDate until = LocalDate.now();
        LocalDate since = until.minusMonths(monthCount);
        return "{since:'" + since.toString() + "',until:'" + until.toString() + "'}";
    }
}
